# Region Growing Temperature Zones (RGTZ) Webservice

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/5e230c25db2d4f82903ab2431904d6e1)](https://www.codacy.com/app/rfmind/RGTZ-Webservice?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=RFmind/RGTZ-Webservice&amp;utm_campaign=Badge_Grade) [![wercker status](https://app.wercker.com/status/1614f634d5431790401e715ecc3c8252/s/master "wercker status")](https://app.wercker.com/project/byKey/1614f634d5431790401e715ecc3c8252)

## Background

RGTZ is a project that helps finding regions in 2D/3D temperature data.
It implements a variation of Region Growing and has (currently very basic) automatic seed location selection. This implementation is exposed as an HTTP webservice (ASP.NET Core).

RGTZ-Webservice is part of a larger solution which also includes a desktop GUI application. That project is developed separately.

RGTZ-Webservice can also be accessed by a cli interface (src/Cli). That interface uses identical business-logic as the web interface (src/Core). This is made easy with Clean Architecture which is based on layers where the Core layer is of the highest abstraction and can be easily reused.

The architecture for this project is based on Clean Architecture (Robert C. Martin) and tries to follow the SOLID principles. You can learn more about it in [his book](https://www.amazon.com/Clean-Architecture-Craftsmans-Software-Structure/dp/0134494164).

## Future Extensions

1. **Better seed location selection**: So far there is a basic solution for the sake of simplicity. There are better ways to approach this problem.

2. **TODO**

## Setup & Run

### Building from source

Make sure you have dotnet core installed on your system.

```
git clone https://gitlab.com/RFmind/RGTZ-Webservice.git
cd RGTZ-Webservice
dotnet restore
dotnet build              # build in Debug mode
dotnet publish -c Release # or build in Release mode
```
- You can now run your application with `dotnet /path/.../to/Webapi.dll`
- You can also choose to run the CLI with `dotnet /path/.../to/Cli.dll`

### Run it as a docker image

**TODO**
