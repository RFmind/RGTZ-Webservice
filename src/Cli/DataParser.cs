using System;

namespace RegionGrowing.Cli
{
    public class DataParser
    {
        public double[,] Parse(string[] lines)
        {
            // TODO: exception handling
            
            var xsize = lines.GetLength(0);
            var ysize = lines[0].Split().GetLength(0);
            double[,] data = new double[xsize, ysize];

            int x = 0;
            int y = 0;

            foreach (var line in lines)
            {
                foreach (var valuestr in line.Split())
                {
                    data[x,y] = Double.Parse(valuestr);
                    y += 1;
                }
                y = 0;
                x += 1;
            }

            return data;
        }
    }
}
