using System;

namespace RegionGrowing.Cli
{
    public class Presenter
    {
        public void present(int[,] data)
        {
            for (int i=0; i<data.GetLength(0); i++)
            {
                var line = "";
                for (int j=0; j<data.GetLength(1); j++)
                {
                    line += data[i,j].ToString();
                    line += " ";
                }
                Console.WriteLine(line);
            }
            Console.WriteLine(" ");
        }
        
        public void present(double[,] data)
        {
            for (int i=0; i<data.GetLength(0); i++)
            {
                var line = "";
                for (int j=0; j<data.GetLength(1); j++)
                {
                    line += data[i,j].ToString();
                    line += " ";
                }
                Console.WriteLine(line);
            }
            Console.WriteLine(" ");
        }
    }
}
