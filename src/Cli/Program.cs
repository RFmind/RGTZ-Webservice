﻿using System;
using McMaster.Extensions.CommandLineUtils;
using RegionGrowing.Core.UseCases;
using RegionGrowing.Core.Requests;
using RegionGrowing.Core.Responses;

namespace RegionGrowing.Cli
{
    class Program
    {
        public static int Main(string[] args)
        {
            var app = new CommandLineApplication();

            app.HelpOption();

            app.Command("hello", helloCmd =>
            {
                helloCmd.OnExecute(() =>
                {
                    Console.WriteLine("Hello World!");
                });
            });

            app.Command("example2D", exampleCmd =>
            {
                exampleCmd.OnExecute(() =>
                {
                    double[,] data = {
                        {1.0, 1.0, 1.0},
                        {2.0, 1.0, 2.0},
                        {3.0, 3.0, 3.0}
                    };

                    int[] seedCoords = {0,0};

                    var request = new GrowRegion2DRequest(data, seedCoords, 0.0, 1);
                    var response = new Grow2DRegion().Handle(request);

                    new Presenter().present(data);
                    new Presenter().present(response.zones);
                });
            });

            app.Command("grow2DRegion", growCmd =>
            {
                var inputpath = growCmd.Option(
                    "-i|--inputpath <path>",
                    "Path to inputfile",
                    CommandOptionType.SingleValue);

                var seedX = growCmd.Option(
                    "-x|--seedX <int>",
                    "X coordinate of the 2D seedpoint",
                    CommandOptionType.SingleValue);

                var seedY = growCmd.Option(
                    "-y|--seedY <int>",
                    "Y coordinate of the 2D seedpoint",
                    CommandOptionType.SingleValue);

                var threshold = growCmd.Option(
                    "-t|--threshold <double>",
                    "Maximum value difference allowed for inclusion to the region",
                    CommandOptionType.SingleValue);

                var zoneNumber = growCmd.Option(
                    "-z|--zonenumber <int>",
                    "Zone number of the region",
                    CommandOptionType.SingleValue);

                growCmd.OnExecute(() =>
                {
                    // TODO: handle exceptions

                    string[] lines = System.IO.File.ReadAllLines(inputpath.Value());
                    double[,] data = new DataParser().Parse(lines);
                    int[] seedCoords = {
                        Int32.Parse(seedX.Value()),
                        Int32.Parse(seedY.Value())};

                    var request = new GrowRegion2DRequest(data, seedCoords, 0.0, 1);
                    var response = new Grow2DRegion().Handle(request);

                    new Presenter().present(response.zones);
                });
            });

            app.Command("grow2DWorld", growCmd =>
            {
                var inputpath = growCmd.Option(
                    "-i|--inputpath <path>",
                    "Path to inputfile",
                    CommandOptionType.SingleValue);

                var seedX = growCmd.Option(
                    "-x|--seedX <int>",
                    "X coordinate of the 2D seedpoint",
                    CommandOptionType.SingleValue);

                var seedY = growCmd.Option(
                    "-y|--seedY <int>",
                    "Y coordinate of the 2D seedpoint",
                    CommandOptionType.SingleValue);

                var threshold = growCmd.Option(
                    "-t|--threshold <double>",
                    "Maximum value difference allowed for inclusion to the region",
                    CommandOptionType.SingleValue);

                var zoneNumber = growCmd.Option(
                    "-z|--zonenumber <int>",
                    "Zone number of the region",
                    CommandOptionType.SingleValue);

                growCmd.OnExecute(() =>
                {
                    // TODO: handle exceptions

                    string[] lines = System.IO.File.ReadAllLines(inputpath.Value());
                    double[,] data = new DataParser().Parse(lines);
                    int[] seedCoords = {
                        Int32.Parse(seedX.Value()),
                        Int32.Parse(seedY.Value())};

                    var request = new GrowWorld2DRequest(data, seedCoords, 0.0, 1);
                    var response = new Grow2DWorld().Handle(request);

                    new Presenter().present(response.zones);
                });
            });


            return app.Execute(args);
        }
    }
}
