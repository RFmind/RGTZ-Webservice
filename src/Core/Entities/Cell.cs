
namespace RegionGrowing.Core.Entities
{
    public class Cell
    {
        public int Zone { get; set; }
        public Location Location { get; set; }
        public double Temperature { get; set; }

        public Cell(Location location)
        {
            this.Zone = -1;
            this.Location = location;
            this.Temperature = 0.0;
        }

        public bool isUnzoned()
        {
            if (this.Zone == -1)
            {
                return true;
            }
            return false;
        }
    }
}
