using System.Collections.Generic;

namespace RegionGrowing.Core.Entities
{
    public class InvalidLocation : Location
    {
        override public bool isInvalid()
        {
            return true;
        }

        public override List<Location> getNeighbors()
        {
            return null;
        }
    }
}
