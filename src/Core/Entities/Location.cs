using System.Collections.Generic;
using System;

namespace RegionGrowing.Core.Entities
{
    public abstract class Location : IEquatable<Location>
    {
        public Dictionary<char,int> Coordinates { get; set; }
        public abstract bool isInvalid();
        public abstract List<Location> getNeighbors();

        public override int GetHashCode()
        {
            var hashcode = 0;

            foreach (var coordinate in this.Coordinates)
            {
                hashcode = hashcode ^ coordinate.Value;
            }
            
            return hashcode;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null) { return false; }

            Location otherLocation = obj as Location;

            foreach (var coordinate in this.Coordinates)
            {
                var otherValue = otherLocation.Coordinates[coordinate.Key];

                if (coordinate.Value != otherValue) { return false; }
            }

            return true;
        }

        public bool Equals(Location location)
        {
            return Equals(location as Object);
        }
    }
}
