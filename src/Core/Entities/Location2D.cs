using System;
using System.Collections.Generic;

namespace RegionGrowing.Core.Entities
{
    public class Location2D : Location
    {
        public Location2D(int x, int y)
        {
            this.Coordinates = new Dictionary<char,int>();
            this.Coordinates.Add('x', x);
            this.Coordinates.Add('y', y);
        }

        public override bool isInvalid()
        {
            return false;
        }
        
        public override string ToString()
        {
            return this.Coordinates['x'].ToString() + this.Coordinates['y'].ToString();
        }

        public bool Equals(Location2D location)
        {
            if (location == null)
            {
                return false;
            }

            return (this.Coordinates['x'] == location.Coordinates['x'])
                && (this.Coordinates['y'] == location.Coordinates['y']);
        }

        public override List<Location> getNeighbors()
        {
            var neighbors = new List<Location>();

            for (int i=-1; i<2; i++)
            {
                for (int j=-1; j<2; j++)
                {
                    if (this.Coordinates['x'] == 0 && i < 0)
                    {
                        continue;
                    }
                    if (this.Coordinates['y'] == 0 && j < 0)
                    {
                        continue;
                    }
                    if (i == 0 && j == 0)
                    {
                        continue;
                    }
                    var x = this.Coordinates['x'] + i;
                    var y = this.Coordinates['y'] + j;
                    neighbors.Add(new Location2D(x,y));
                }
            }
 
            return neighbors;
        }
    }
}
