using System;
using System.Collections.Generic;

namespace RegionGrowing.Core.Entities
{
    public class Location3D : Location
    {
        public Location3D(int x, int y, int z)
        {
            this.Coordinates = new Dictionary<char,int>();
            this.Coordinates.Add('x', x);
            this.Coordinates.Add('y', y);
            this.Coordinates.Add('z', z);
        }

        public override bool isInvalid()
        {
            return false;
        }

        public override string ToString()
        {
            return this.Coordinates['x'].ToString() + this.Coordinates['y'].ToString() + this.Coordinates['z'].ToString();
        }

        public bool Equals(Location3D location)
        {
            if (location == null)
            {
                return false;
            }

            return (this.Coordinates['x'] == location.Coordinates['x'])
                && (this.Coordinates['y'] == location.Coordinates['y'])
                && (this.Coordinates['z'] == location.Coordinates['z']);
        }

        public override List<Location> getNeighbors()
        {
            var neighbors = new List<Location>();

            for (int i=-1; i<2; i++)
            {
                for (int j=-1; j<2; j++)
                {
                    for (int k=-1; k<2; k++)
                    {
                        var xDoesntExist = this.Coordinates['x'] == 0 && i < 0;
                        var yDoesntExist = this.Coordinates['y'] == 0 && j < 0;
                        var zDoesntExist = this.Coordinates['z'] == 0 && k < 0;

                        if (xDoesntExist || yDoesntExist || zDoesntExist)
                        {
                            continue;
                        }

                        var x = this.Coordinates['x'] + i;
                        var y = this.Coordinates['y'] + j;
                        var z = this.Coordinates['z'] + k;
                        neighbors.Add(new Location3D(x,y,z));
                    }
                }
            }

            return neighbors;
        }
    }
}
