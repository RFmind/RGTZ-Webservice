using System.Collections.Generic;
using System;

namespace RegionGrowing.Core.Entities
{
    public class RegionMaker
    {
        public (World, Location) GrowRegion(
            World world,
            Location seedLocation,
            double threshold,
            int zone)
        {

            var queue = new Queue<Cell>();
            var seed = world.GetCell(seedLocation);
            Location nextSeedLocation = new InvalidLocation();

            queue.Enqueue(seed);
            while (queue.Count > 0)
            {
                var cell = queue.Dequeue();

                if (this.CellInRegion(cell, seed, threshold))
                {
                    world.AssignZoneTo(cell.Location, zone);

                    var neighbors = world.GetUnzonedNeighborsFor(
                        cell.Location);

                    foreach (Cell neighbor in neighbors)
                    {
                        queue.Enqueue(neighbor);
                    }
                    continue;
                }

                if (nextSeedLocation.isInvalid())
                {
                    nextSeedLocation = cell.Location;
                }
            }

            if (nextSeedLocation.isInvalid())
            {
                var unzonedCell = world.GetFirstUnzonedCell();

                if (unzonedCell != null)
                {
                    nextSeedLocation = unzonedCell.Location;
                }
            }

            return (world, nextSeedLocation);
        }

        public World GrowWorld(
            World world,
            Location seedLocation,
            double threshold,
            int zone)
        {

            var (newWorld, newSeedLocation) = GrowRegion(
                world,
                seedLocation,
                threshold,
                zone);

            while (newSeedLocation.isInvalid() == false)
            {
                zone += 1;
                (newWorld, newSeedLocation) = GrowRegion(
                    newWorld,
                    newSeedLocation,
                    threshold,
                    zone);
            }

            return newWorld;
        }

        private bool CellInRegion(Cell cell, Cell seed, double threshold)
        {
            if (Math.Abs(cell.Temperature - seed.Temperature) <= threshold)
            {
                return true;
            }
            return false;
        }
    }
}
