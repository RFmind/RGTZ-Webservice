using System.Collections.Generic;

namespace RegionGrowing.Core.Entities
{
    public class Size
    {
        public List<int> Values { get; set; }

        public void set2D(int x, int y)
        {
            if (this.Values.Count == 0)
            {
                this.Values.Add(x);
                this.Values.Add(y);
            }
        }

        public void set3D(int x, int y, int z)
        {
            if (this.Values.Count == 0)
            {
                this.Values.Add(x);
                this.Values.Add(y);
                this.Values.Add(z);
            }
        }

        public Size()
        {
            this.Values = new List<int>();
        }
    }
}
