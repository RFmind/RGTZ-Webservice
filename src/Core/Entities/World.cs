using System.Collections.Generic;

namespace RegionGrowing.Core.Entities
{
    public class World
    {
        public Dictionary<Location, Cell> Grid { get; set; }
        public Size Size { get; set; }

        public void AssignZoneTo(Location location, int zone)
        {
            this.Grid[location].Zone = zone;
        }

        public List<Cell> GetUnzonedNeighborsFor(Location location)
        {
            var neighbors = new List<Cell>();

            foreach (Location neighborLocation in location.getNeighbors())
            {
                if (this.hasLocation(neighborLocation))
                {
                    var neighbor = this.Grid[neighborLocation];
                    
                    if (neighbor.isUnzoned())
                    {
                        neighbors.Add(neighbor);
                    }
                }
            }

            return neighbors;
        }

        public Cell GetFirstUnzonedCell()
        {
            foreach (KeyValuePair<Location,Cell> pair in this.Grid)
            {
                Cell cell = pair.Value;
                
                if (cell.isUnzoned())
                {
                    return cell;
                }
            }
            return null;
        }

        public bool HasZones()
        {
            foreach (KeyValuePair<Location,Cell> pair in this.Grid)
            {
                Cell cell = pair.Value;

                if (!cell.isUnzoned())
                {
                    return true;
                }
            }
            return false;
        }

        public Cell GetCell(Location location)
        {
            return this.Grid[location];
        }

        private bool hasLocation(Location location)
        {
            return this.Grid.ContainsKey(location);
        }
    }
}
