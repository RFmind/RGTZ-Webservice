using System.Collections.Generic;

namespace RegionGrowing.Core.Entities
{
    public class World2D : World
    {
        public World2D(Size size)
        {
            this.Grid = new Dictionary<Location, Cell>();
            this.Size = size;

            for (int i = 0; i < size.Values[0]; i++)
            {
                for (int j = 0; j < size.Values[1]; j++)
                {
                    var location = new Location2D(i,j);
                    var cell = new Cell(location);
                    this.Grid.Add(location, cell);
                }
            }
        }

        public void LoadData(double[,] data)
        {
            for (int i = 0; i < data.GetLength(0); i++)
            {
                for (int j = 0; j < data.GetLength(1); j++)
                {
                    var location = new Location2D(i,j);
                    this.Grid[location].Temperature = data[i,j];
                    this.Grid[location].Zone = -1;
                }
            }
        }
    }
}
