using System.Collections.Generic;

namespace RegionGrowing.Core.Entities
{
    public class World3D : World
    {
        public World3D(Size size)
        {
            this.Grid = new Dictionary<Location, Cell>();
            this.Size = size;

            for (int i=0; i<size.Values[0]; i++)
            {
                for (int j=0; j<size.Values[1]; j++)
                {
                    for (int k=0; k<size.Values[2]; k++)
                    {
                        var location = new Location3D(i,j,k);
                        var cell = new Cell(location);
                        this.Grid.Add(location, cell);
                    }
                }
            }
        }

        public void LoadData(double[,,] data)
        {
            for (int i=0; i<data.GetLength(0); i++)
            {
                for (int j=0; j<data.GetLength(1); j++)
                {
                    for (int k=0; k<data.GetLength(2); k++)
                    {
                        var location = new Location3D(i,j,k);
                        this.Grid[location].Temperature = data[i,j,k];
                        this.Grid[location].Zone = -1;
                    }
                }
            }
        }
    }
}
