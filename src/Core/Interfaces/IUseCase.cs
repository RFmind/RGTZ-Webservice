using RegionGrowing.Core.Requests;
using RegionGrowing.Core.Responses;

namespace RegionGrowing.Core.Interfaces
{
    public interface IUseCase<in T, out U>
        where T : CoreRequest
        where U : CoreResponse
    {
        U Handle(T coreRequest);
    }
}
