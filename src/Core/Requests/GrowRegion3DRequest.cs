using RegionGrowing.Core.Entities;

namespace RegionGrowing.Core.Requests
{
    public class GrowRegion3DRequest : CoreRequest
    {
        public World3D world { get; }
        public Location3D seedLocation { get; }
        public double threshold { get; }
        public int zone { get; }

        public GrowRegion3DRequest(
            double[,,] world,
            int[] seedCoords,
            double threshold,
            int zone)
        {
            var size = new Size();
            size.set3D(world.GetLength(0), world.GetLength(1), world.GetLength(2));
            
            this.seedLocation = new Location3D(seedCoords[0],seedCoords[1],seedCoords[2]);
            this.world = new World3D(size);
            this.world.LoadData(world);
            this.threshold = threshold;
            this.zone = zone;
        }
    }
}