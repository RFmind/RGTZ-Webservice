using RegionGrowing.Core.Entities;

namespace RegionGrowing.Core.Requests
{
    public class GrowWorld2DRequest : CoreRequest
    {
        public World2D world { get; }
        public Location2D seedLocation { get; }
        public double threshold { get; }
        public int zone { get; }

        public GrowWorld2DRequest(
            double[,] world,
            int[] seedCoords,
            double threshold,
            int zone)
        {
            var size = new Size();
            size.set2D(world.GetLength(0), world.GetLength(1));

            this.seedLocation = new Location2D(seedCoords[0], seedCoords[1]);
            this.world = new World2D(size);
            this.world.LoadData(world);
            this.threshold = threshold;
            this.zone = zone;
        }
    }
}
