using RegionGrowing.Core.Entities;
using System;

namespace RegionGrowing.Core.Responses
{
    public class RegionGrown2D : CoreResponse
    {
        public double[,] world { get; }
        public int[,] zones { get; }
        
        public RegionGrown2D(World2D world)
        {
            var maxX = world.Size.Values[0];
            var maxY = world.Size.Values[1];
            this.world = new double[maxX,maxY];
            this.zones = new int[maxX,maxY];

            foreach (var pair in world.Grid)
            {
                var x = pair.Key.Coordinates['x'];
                var y = pair.Key.Coordinates['y'];
                var temp = pair.Value.Temperature;
                var zone = pair.Value.Zone;
                this.world[x,y] = temp;
                this.zones[x,y] = zone;
            }
        }
    }
}
