using System;
using RegionGrowing.Core.Entities;

namespace RegionGrowing.Core.Responses
{
    public class RegionGrown3D : CoreResponse
    {
        public double[,,] world { get; }
        public int[,,] zones { get; }

        public RegionGrown3D(World3D world)
        {
            var maxX = world.Size.Values[0];
            var maxY = world.Size.Values[1];
            var maxZ = world.Size.Values[2];

            this.world = new double[maxX,maxY,maxZ];
            this.zones = new int[maxX,maxY,maxZ];

            foreach (var pair in world.Grid)
            {
                var x = pair.Key.Coordinates['x'];
                var y = pair.Key.Coordinates['y'];
                var z = pair.Key.Coordinates['z'];

                var temp = pair.Value.Temperature;
                var zone = pair.Value.Zone;

                this.world[x,y,z] = temp;
                this.zones[x,y,z] = zone;
            }
        }
    }
}