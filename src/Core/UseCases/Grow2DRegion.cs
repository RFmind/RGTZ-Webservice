using RegionGrowing.Core.Interfaces;
using RegionGrowing.Core.Entities;
using RegionGrowing.Core.Requests;
using RegionGrowing.Core.Responses;

namespace RegionGrowing.Core.UseCases
{
    public class Grow2DRegion : IUseCase<GrowRegion2DRequest,RegionGrown2D>
    {
        public RegionGrown2D Handle(GrowRegion2DRequest coreRequest)
        {
            var regionMaker = new RegionMaker();
            (var newWorld, var newSeedLocation) = regionMaker.GrowRegion(
                coreRequest.world,
                coreRequest.seedLocation,
                coreRequest.threshold,
                coreRequest.zone);
            var response = new RegionGrown2D(newWorld as World2D);

            return response;
        }
    }
}
