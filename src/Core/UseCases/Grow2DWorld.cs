using RegionGrowing.Core.Interfaces;
using RegionGrowing.Core.Entities;
using RegionGrowing.Core.Requests;
using RegionGrowing.Core.Responses;

namespace RegionGrowing.Core.UseCases
{
    public class Grow2DWorld : IUseCase<GrowWorld2DRequest, WorldGrown2D>
    {
        public WorldGrown2D Handle(GrowWorld2DRequest coreRequest)
        {
            var regionMaker = new RegionMaker();
            var newWorld = regionMaker.GrowWorld(
                coreRequest.world,
                coreRequest.seedLocation,
                coreRequest.threshold,
                coreRequest.zone);
            var response = new WorldGrown2D(newWorld as World2D);

            return response;
        }
    }
}
