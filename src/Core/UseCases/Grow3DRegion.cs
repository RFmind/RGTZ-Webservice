using RegionGrowing.Core.Interfaces;
using RegionGrowing.Core.Entities;
using RegionGrowing.Core.Requests;
using RegionGrowing.Core.Responses;

namespace RegionGrowing.Core.UseCases
{
    public class Grow3DRegion : IUseCase<GrowRegion3DRequest,RegionGrown3D>
    {
        public RegionGrown3D Handle(GrowRegion3DRequest request)
        {
            var regionMaker = new RegionMaker();
            (var newWorld, var newSeedLocation) = regionMaker.GrowRegion(
                request.world,
                request.seedLocation,
                request.threshold,
                request.zone);
            var response = new RegionGrown3D(newWorld as World3D);

            return response;
        }
    }
}