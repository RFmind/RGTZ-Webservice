using System.Collections.Generic;
using RegionGrowing.Persistence.Models;

namespace RegionGrowing.Persistence.Interfaces
{
    public interface IRepository
    {
        void Add(GrownWorld grownworld);

        List<GrownWorld> GetAll();

        GrownWorld Get(int id);
    }
}
