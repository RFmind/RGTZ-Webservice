using System;

namespace RegionGrowing.Persistence.Models
{
    public class GrownWorld
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; }
        public double[,] World { get; set; }
        public int[,] Zones { get; set; }

        public GrownWorld(double[,] world, int[,] zones)
        {
            Id = -1;
            CreatedAt = DateTime.Now;
            World = world;
            Zones = zones;
        }
    }
}