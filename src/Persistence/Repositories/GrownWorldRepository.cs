using System;
using System.Collections.Generic;
using RegionGrowing.Persistence.Models;
using RegionGrowing.Persistence.Interfaces;

namespace RegionGrowing.Persistence.Repositories
{
    public class GrownWorldRepository : IRepository
    {
        private readonly List<GrownWorld> _grownWorlds;

        public GrownWorldRepository()
        {
            _grownWorlds = new List<GrownWorld>();
        }

        public void Add(GrownWorld grownworld)
        {
            if (_grownWorlds.Count >= 10)
            {
                garbageCollectCache();
            }
            grownworld.Id = _grownWorlds.Count;
            _grownWorlds.Add(grownworld);
        }

        public GrownWorld Get(int id)
        {
            return _grownWorlds[id];
        }

        public List<GrownWorld> GetAll()
        {
            return _grownWorlds;
        }

        private void garbageCollectCache()
        {
            int currentId = 0;
            foreach (GrownWorld grownworld in _grownWorlds)
            {
                if (DateTime.Now.Subtract(grownworld.CreatedAt).TotalMinutes > 5.0)
                {
                    _grownWorlds.RemoveAt(currentId);
                }
                currentId += 1;
            }
        }
    }
}