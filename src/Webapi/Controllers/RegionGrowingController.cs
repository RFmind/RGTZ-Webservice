using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RegionGrowing.Core.UseCases;
using RegionGrowing.Core.Requests;
using RegionGrowing.Core.Responses;
using RegionGrowing.Persistence.Interfaces;
using RegionGrowing.Persistence.Models;

namespace Webapi.Controllers
{
    [Route("api/region/[controller]")]
    public class RegionGrowingController : Controller
    {
        private IRepository _repository;

        public RegionGrowingController(IRepository repo)
        {
            _repository = repo;
        }

        public void Post([FromBody]GrowRegion2DRequest request)
        {
            var response = new Grow2DRegion().Handle(request);
            var grownworld = new GrownWorld(response.world, response.zones);
            _repository.Add(grownworld);
        }

        [HttpGet("{id}")]
        public GrownWorld Get(int id)
        {
            var grownworld = _repository.Get(id);
            return grownworld;
        }
    }
}
