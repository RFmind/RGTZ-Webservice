using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RegionGrowing.Core.UseCases;
using RegionGrowing.Core.Requests;
using RegionGrowing.Core.Responses;
using RegionGrowing.Persistence.Interfaces;
using RegionGrowing.Persistence.Models;

namespace Webapi.Controllers
{
    [Route("api/world/[controller]")]
    public class WorldGrowingController : Controller
    {
        private IRepository _repository;

        public WorldGrowingController(IRepository repo)
        {
            _repository = repo;
        }

        public void Post([FromBody]GrowWorld2DRequest request)
        {
            var response = new Grow2DWorld().Handle(request);
            var grownworld = new GrownWorld(response.world, response.zones);
            _repository.Add(grownworld);
        }

        [HttpGet("{id}")]
        public GrownWorld Get(int id)
        {
            var grownworld = _repository.Get(id);
            return grownworld;
        }
    }
}
