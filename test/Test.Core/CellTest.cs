using Xunit;
using RegionGrowing.Core.Entities;

namespace RegionGrowing.Test.Core.Entities
{
    public class CellTest
    {
        [Fact]
        public void HasZone()
        {
            var cell = new Cell(new Location2D(1,2));
            cell.Zone = 1;
            Assert.True(cell.Zone == 1);
        }

        [Fact]
        public void HasTemperature()
        {
            var cell = new Cell(new Location2D(1,2));
            cell.Temperature = 12.2;
            Assert.True(cell.Temperature == 12.2);
        }

        [Fact]
        public void IsUnzonedByDefault()
        {
            var cell = new Cell(new Location2D(1,2));
            Assert.True(cell.isUnzoned());
        }

        [Fact]
        public void NotUnzonedIfZoneSet()
        {
            var cell = new Cell(new Location2D(1,2));
            cell.Zone = 1;

            Assert.False(cell.isUnzoned());
        }
    }
}
