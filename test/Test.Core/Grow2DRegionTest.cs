using Xunit;
using RegionGrowing.Core.UseCases;
using RegionGrowing.Core.Requests;
using RegionGrowing.Core.Responses;

namespace RegionGrowing.Test.Core.UseCases
{
    public class Grow2DRegionTest
    {
        [Fact]
        public void GrowsRegionGivenCorrectRequest()
        {
            double[,] world = {
                {1.0, 1.0, 1.0},
                {1.0, 1.0, 1.0},
                {1.0, 1.0, 1.0}
            };

            int[] seedCoords = {0,0};
            var request = new GrowRegion2DRequest(world, seedCoords, 0.0, 1);
            var response = new Grow2DRegion().Handle(request);
            
            Assert.True(response.world != null);
            Assert.True(response.zones[0,0] == 1);
            Assert.True(response.zones[0,1] == 1);
        }
    }
}
