using Xunit;
using RegionGrowing.Core.UseCases;
using RegionGrowing.Core.Requests;
using RegionGrowing.Core.Responses;

namespace RegionGrowing.Test.Core.UseCases
{
    public class Grow2Grow2DTest
    {
        [Fact]
        public void GrowsWorldGivenCorrectRequest()
        {
            double[,] world = {
                {1.0, 1.0, 1.0},
                {1.0, 2.0, 2.0},
                {1.0, 2.0, 2.0}
            };

            int[] seedCoords = {0,0};
            var request = new GrowWorld2DRequest(world, seedCoords, 0.0, 1);
            var response = new Grow2DWorld().Handle(request);

            Assert.True(response.world != null);
            Assert.True(response.zones[0,0] == 1);
            Assert.True(response.zones[0,1] == 1);
            Assert.True(response.zones[1,1] == 2);
            Assert.True(response.zones[1,2] == 2);
        }
    }
}
