using Xunit;
using RegionGrowing.Core.UseCases;
using RegionGrowing.Core.Requests;
using RegionGrowing.Core.Responses;

namespace RegionGrowing.Test.Core.UseCases
{
    public class Grow3DRegionTest
    {
        [Fact]
        public void GrowRegionGivenCorrectRequest()
        {
            double[,,] data = {
                {{1.0,1.0}, {1.0,1.0}},
                {{1.0,1.0}, {1.0,1.0}}
            };

            int[] seedCoords = {0,0,0};
            var request = new GrowRegion3DRequest(data, seedCoords, 0.0, 1);
            var response = new Grow3DRegion().Handle(request);

            Assert.True(response.world != null);
            Assert.True(response.zones[0,0,0] == 1);
            Assert.True(response.zones[0,1,0] == 1);
        }
    }
}