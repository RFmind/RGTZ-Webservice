using Xunit;
using RegionGrowing.Core.Entities;

namespace RegionGrowing.Test.Core.Entities
{
    public class InvalidLocationTest
    {
        [Fact]
        public void IsInvalid()
        {
            var location = new InvalidLocation();
            Assert.True(location.isInvalid());
        }

        [Fact]
        public void HasNoNeighbors()
        {
            var location = new InvalidLocation();
            Assert.True(location.getNeighbors() == null);
        }
    }
}
