using Xunit;
using System.Collections.Generic;
using RegionGrowing.Core.Entities;

namespace RegionGrowing.Test.Core.Entities
{
    public class Location2DTest
    {
        [Fact]
        public void IsAValidLocation()
        {
            var location = new Location2D(1,2);
            Assert.False(location.isInvalid());
        }

        [Fact]
        public void getNeighborsReturnsPositiveNeighborLocations()
        {
            var location1 = new Location2D(0,0);
            var neighbors = location1.getNeighbors();
            
            foreach (Location2D neighbor in neighbors)
            {
                foreach (KeyValuePair<char,int> coordinate in neighbor.Coordinates)
                {
                    Assert.True(coordinate.Value >= 0);
                }
            }
        }

        [Fact]
        public void twoLocationsWithEqualCoordinatesAreEqual()
        {
            var location1 = new Location2D(1,2);
            var location2 = new Location2D(1,2);

            Assert.True(location1.Equals(location2));
        }

        [Fact]
        public void LocationsWithEqualCoordinatesHaveEqualHashCode()
        {
            var location1 = new Location2D(1,2);
            var location2 = new Location2D(1,2);

            Assert.True(location1.GetHashCode() == location2.GetHashCode());
        }
    }
}
