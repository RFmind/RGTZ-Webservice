using Xunit;
using System.Collections.Generic;
using RegionGrowing.Core.Entities;

namespace RegionGrowing.Test.Core.Entities
{
    public class Location3DTest
    {
        [Fact]
        public void IsAValidLocation()
        {
            var location = new Location3D(1,2,3);
            Assert.False(location.isInvalid());
        }

        [Fact]
        public void getNeighborsReturnsPositiveNeighborLocations()
        {
            var location1 = new Location3D(1,2,3);
            var neighbors = location1.getNeighbors();

            foreach (var neighbor in neighbors)
            {
                foreach (var coordinate in neighbor.Coordinates)
                {
                    Assert.True(coordinate.Value >= 0);
                }
            }
        }

        [Fact]
        public void twoLocationsWithEqualCoorinatesAreEqual()
        {
            var location1 = new Location3D(1,2,3);
            var location2 = new Location3D(1,2,3);

            Assert.True(location1.Equals(location2));
        }

        [Fact]
        public void LocationsWithEqualCoordinatesHaveEqualHashCode()
        {
            var location1 = new Location3D(1,2,3);
            var location2 = new Location3D(1,2,3);

            Assert.True(location1.GetHashCode() == location2.GetHashCode());
        }
    }
}