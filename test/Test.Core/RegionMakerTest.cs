using Xunit;
using RegionGrowing.Core.Entities;

namespace RegionGrowing.Test.Core.Entities
{
    public class RegionMakerTest
    {
        [Fact]
        public void GrowRegionGrowsRegionCorrectly()
        {
            var size = new Size();
            size.set2D(4,4);
            var exampleWorld = new World2D(size);
            var data = new double[,] {
                {1.0,1.0,1.0,2.0},
                {1.0,2.0,2.0,3.0},
                {1.0,2.0,3.0,3.0}
            };
            exampleWorld.LoadData(data);

            var seedLocation = new Location2D(0,0);
            
            var regionMaker = new RegionMaker();
            (var newWorld, var newSeedLocation) = regionMaker.GrowRegion(
                exampleWorld,
                seedLocation,
                0.0,
                1);

            Assert.False(newWorld.GetCell(seedLocation).isUnzoned());
            Assert.True(newWorld.GetCell(seedLocation).Zone == 1);
            Assert.True(newWorld.GetCell(new Location2D(0,1)).Zone == 1);
            Assert.True(newWorld.GetCell(new Location2D(3,3)).isUnzoned());
        }
    }
}
