using Xunit;
using System.Collections.Generic;
using RegionGrowing.Core.Entities;

namespace RegionGrowing.Test.Core.Entities
{
    public class SizeTest
    {

        [Fact]
        public void NewSizeHasNoValues()
        {
            var size = new Size();
            Assert.True(size.Values.Count == 0);
        }

        [Fact]
        public void SetSize2DAddsTwoValuesOnce()
        {
            var size = new Size();
            size.set2D(1,2);

            Assert.True(size.Values[0] == 1);
            Assert.True(size.Values[1] == 2);
            Assert.True(size.Values.Count == 2);

            size.set2D(3,4);
            Assert.True(size.Values[0] == 1);
            Assert.True(size.Values.Count == 2);
        }

        [Fact]
        public void SetSize3DAddsThreeValuesOnce()
        {
            var size = new Size();
            size.set3D(1,2,3);

            Assert.True(size.Values[0] == 1);
            Assert.True(size.Values[1] == 2);
            Assert.True(size.Values.Count == 3);

            size.set3D(5,6,7);
            Assert.True(size.Values.Count == 3);
            Assert.True(size.Values[0] == 1);
        }
    }
}
