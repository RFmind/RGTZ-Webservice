using Xunit;
using RegionGrowing.Core.Entities;

namespace RegionGrowing.Test.Core.Entities
{
    public class World2DTest
    {
        [Fact]
        public void ConstructorTakesSize()
        {
            var size = new Size();
            size.set2D(2,2);
            var world2D = new World2D(size);

            Assert.True(world2D.Size == size);
        }

        [Fact]
        public void WorldIsUnzonedByDefault()
        {
            var size = new Size();
            size.set2D(2,2);
            var world2D = new World2D(size);

            Assert.True(world2D.GetCell(new Location2D(0,0)).isUnzoned());
            Assert.True(world2D.GetCell(new Location2D(0,1)).isUnzoned());
        }

        [Fact]
        public void LoadDataUpdatesTemperatureData()
        {
            var size = new Size();
            size.set2D(2,2);
            var world2D = new World2D(size);
            double[,] newdata = {{1.0,1.0},{1.0,1.0}};

            Assert.True(world2D.GetCell(new Location2D(0,0)).Temperature == 0.0);

            world2D.LoadData(newdata);

            Assert.True(world2D.GetCell(new Location2D(0,0)).Temperature == 1.0);
        }

        [Fact]
        public void LoadDataResetsAllZones()
        {
            var size = new Size();
            size.set2D(2,2);
            var world2D = new World2D(size);
            double[,] data = {{1.0,1.0},{1.0,1.0}};
            var location = new Location2D(0,0);

            world2D.AssignZoneTo(location, 15);
            Assert.True(world2D.GetCell(location).Zone == 15);

            world2D.LoadData(data);
            Assert.True(world2D.GetCell(location).Zone == -1);
            Assert.False(world2D.GetCell(location).Zone == 15);
        }
    }
}