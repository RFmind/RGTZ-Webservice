using Xunit;
using RegionGrowing.Core.Entities;

namespace RegionGrowing.Test.Core.Entities
{
    public class World3DTest
    {
        [Fact]
        public void ConstructorTakesSize()
        {
            var size = new Size();
            size.set3D(2,2,2);
            var world3D = new World3D(size);

            Assert.True(world3D.Size == size);
        }

        [Fact]
        public void WorldIsUnzonedByDefault()
        {
            var size = new Size();
            size.set3D(2,2,2);
            var world3D = new World3D(size);

            Assert.True(world3D.GetCell(new Location3D(0,0,0)).isUnzoned());
            Assert.True(world3D.GetCell(new Location3D(0,0,1)).isUnzoned());
        }

        [Fact]
        public void LoadDataUpdatesTemperatureData()
        {
            var size = new Size();
            size.set3D(2,2,2);
            var world3D = new World3D(size);
            double[,,] newdata = {
                {{1.0,1.0}, {1.0,1.0}},
                {{1.0,1.0}, {1.0,1.0}}
            };
            var location1 = new Location3D(0,0,0);
            var location2 = new Location3D(0,0,1);

            Assert.True(world3D.GetCell(location1).Temperature == 0.0);
            Assert.True(world3D.GetCell(location2).Temperature == 0.0);

            world3D.LoadData(newdata);

            Assert.True(world3D.GetCell(location1).Temperature == 1.0);
            Assert.True(world3D.GetCell(location2).Temperature == 1.0);
        }

        [Fact]
        public void LoadDataResetsAllZones()
        {
            var size = new Size();
            size.set3D(2,2,2);
            var world3D = new World3D(size);
            double[,,] data = {
                {{1.0,1.0}, {1.0,1.0}},
                {{1.0,1.0}, {1.0,1.0}}
            };
            var location = new Location3D(0,0,0);

            world3D.AssignZoneTo(location, 15);
            Assert.True(world3D.GetCell(location).Zone == 15);

            world3D.LoadData(data);
            Assert.True(world3D.GetCell(location).Zone == -1);
        }
    }
}