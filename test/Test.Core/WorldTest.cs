using Xunit;
using RegionGrowing.Core.Entities;

namespace RegionGrowing.Test.Core.Entities
{
    public class WorldTest
    {
        [Fact]
        public void AssignZoneToUpdatesZone()
        {
            var size = new Size();
            size.set2D(2,2);
            World world = new World2D(size);
            var location = new Location2D(0,1);

            Assert.True(world.GetCell(location).isUnzoned());
            Assert.True(world.GetCell(location).Zone == -1);

            world.AssignZoneTo(location, 15);

            Assert.True(world.GetCell(location).Zone == 15);
            Assert.False(world.GetCell(location).Zone == -1);
        }

        [Fact]
        public void GetUnzonedNeighborsForReturnsCorrectNeighbors()
        {
            var size = new Size();
            size.set2D(2,2);
            World world = new World2D(size);
            var location = new Location2D(0,0);

            var neighbors = world.GetUnzonedNeighborsFor(location);
            Assert.True(neighbors.Count == 3);

            world.AssignZoneTo(new Location2D(0,1), 1);
            neighbors = world.GetUnzonedNeighborsFor(location);
            Assert.True(neighbors.Count == 2);
        }

        [Fact]
        public void GetFirstUnzonedCellReturnsAnUnzonedCell()
        {
            var size = new Size();
            size.set2D(2,2);
            World world = new World2D(size);

            Assert.True(world.GetFirstUnzonedCell().isUnzoned());
        }

        [Fact]
        public void GetFirstUnzonedCellReturnsNullWhenNoUnzonedCell()
        {
            var size = new Size();
            size.set2D(2,2);
            World world = new World2D(size);
            world.AssignZoneTo(new Location2D(0,0), 1);
            world.AssignZoneTo(new Location2D(0,1), 1);
            world.AssignZoneTo(new Location2D(1,0), 1);
            world.AssignZoneTo(new Location2D(1,1), 1);

            Assert.True(world.GetFirstUnzonedCell() == null);
        }

        [Fact]
        public void HasZonesChecksForZones()
        {
            var size = new Size();
            size.set2D(2,2);
            World world = new World2D(size);

            Assert.False(world.HasZones());

            world.AssignZoneTo(new Location2D(0,0), 1);

            Assert.True(world.HasZones());
        }
    }
}