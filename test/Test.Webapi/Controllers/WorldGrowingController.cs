using Xunit;
using Webapi.Controllers;
using RegionGrowing.Test.Webapi.Mocks;
using RegionGrowing.Persistence.Interfaces;
using RegionGrowing.Persistence.Models;
using RegionGrowing.Core.Requests;

namespace RegionGrowing.Test.Webapi.Controllers
{
    public class WorldGrowingControllerTest
    {
        [Fact]
        public void GetReturnsCorrectlyGivenExistingId()
        {
            var controller = new WorldGrowingController(new TestRepository());
            var grownworld = controller.Get(0);

            Assert.True(grownworld.World != null);
            Assert.True(grownworld.Zones != null);
        }

        [Fact]
        public void PostAddsAGrownWorld()
        {
            var repo = new TestRepository();

            Assert.True(repo._grownworlds.Count == 5);

            var controller = new WorldGrowingController(repo);
            double[,] world = {
                {1.0,1.0,1.0},
                {1.0,1.0,1.0},
                {1.0,1.0,1.0}
            };
            int[] seedCoords = {0,0};

            var request = new GrowWorld2DRequest(
                world,
                seedCoords,
                0.0,
                1);

            controller.Post(request);

            Assert.True(repo._grownworlds.Count == 6);
        }

        [Fact]
        public void AddedGrownWorldIsGettable()
        {
            var repo = new TestRepository();

            Assert.True(repo._grownworlds.Count == 5);

            var controller = new WorldGrowingController(repo);
            double[,] world = {
                {1.0,2.0},
                {1.0,2.0}
            };
            int[] seedCoords = {0,0};

            var request = new GrowWorld2DRequest(
                world,
                seedCoords,
                0.0,
                1);

            controller.Post(request);

            Assert.True(controller.Get(5) != null);
            Assert.True(controller.Get(5).World != null);
            Assert.True(controller.Get(5).World.GetLength(0) == world.GetLength(0));
            Assert.True(controller.Get(5).Zones != null);
        }
    }
}
