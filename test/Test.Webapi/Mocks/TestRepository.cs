using System.Collections.Generic;
using RegionGrowing.Persistence.Interfaces;
using RegionGrowing.Persistence.Models;

namespace RegionGrowing.Test.Webapi.Mocks
{
    public class TestRepository : IRepository
    {
        public List<GrownWorld> _grownworlds { get; set; }

        public TestRepository()
        {
            _grownworlds = new List<GrownWorld>();

            double[,] world = {
                {1.0, 1.0, 1.0},
                {1.0, 1.0, 1.0},
                {1.0, 1.0, 1.0}
            };
            int[,] zones = {
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
            };
            for (int i = 0; i<5; i++)
            {
                _grownworlds.Add(new GrownWorld(world, zones));
            }
        }

        public void Add(GrownWorld grownworld)
        {
            _grownworlds.Add(grownworld);
        }

        public GrownWorld Get(int id)
        {
            return _grownworlds[id];
        }

        public List<GrownWorld> GetAll()
        {
            return _grownworlds;
        }
    }
}